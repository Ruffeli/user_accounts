class Users {
	/*
		Fetch userdata from REST API
		list users
		remove, add and modify a user 
	*/

	constructor(url) {
		this.userlist = [];
		this.apiUrl = 'https://jsonplaceholder.typicode.com/users'; //Url for REST API
		this.editCells = '<td class="text-center"><a class="edit" href="#"><span class="glyphicon glyphicon-pencil"></span></a></td><td class="text-center"><a class="delete" href="#"><span class="glyphicon glyphicon-trash"></span></a></td>';
		if(typeof url != "undefined" && url.length > 0) {
			this.apiUrl = url;
		}
	}

	getUsers(element) {
		//Retrieve data from API
		var _this = this; //Save reference to class
		$.getJSON(this.apiUrl + "?callback=?", function(data) {
			_this.userlist = data;
			_this.listUsers(element); //List data to user
			_this.attachUserEvents(); //Attach DOM events
			table = $(element).DataTable({
				responsive: true,
				columns: [
					{responsivePriority: 0},
					null,
					null,
					{
						responsivePriority: 1,
						"orderable": false
					},
					{
						responsivePriority: 2,
						"orderable": false
					}
				]
			}); //Make table searchable and sortable

		}).fail(function() {
			//something went wrong
			putAlert("Error loading users!", 'danger');
		});
	}

	listUsers(element) {
		//List users
		//Appends userlist to the end of the <element>
		var users = [];
		var fields = ['name', 'username', 'email']; //Fields we want to show in listing
		var cells = '';
		var _this = this;

		$.each(this.userlist, function(key, val) {
			cells = '';
			$.each(fields, function(fkey, fval) {
				cells += "<td class='" + fval + "'>" + eval('val.' + fval) + "</td>";
			});
			users.push("<tr data='" + val.id + "'>" + cells + _this.editCells + "</tr>");
		});
//		var html = "<ul>" + users.join("") + "</ul>";
		var html = users.join("");
		$(element + '> tbody').append(html);
	}

	deleteUser(id) {
		//Remove user from list and database
		var row = $('#userlist tr[data='+ id +']');
		console.log(row);
		$.ajax(this.apiUrl + '/' + id, {
			method: 'DELETE'
		}).done(function() {
			putAlert("User removed", 'info');
			table.row(row).remove().draw();
		}).fail(function() {
			putAlert("Error deleting user!", 'danger');
		});
	}

	attachRowEvents() {
		$('.delete').click($.proxy(function(event) {
			//Handle delete icon click
			event.preventDefault();
			var row = $(event.currentTarget).parents("tr"); // Find current row
			var id = row.attr("data");
			var username = row.find(".username").text();
			$('#modalUsername').text(username);
			$('#deleteUser').attr('data', id);
			$('#delModal').modal();

			//$.proxy(this.deleteUser, this);
		}, this));

		$('.edit').click($.proxy(function(event) {
			event.preventDefault();
			var row = $(event.currentTarget).parents("tr"); // Find current row
			var id = row.attr("data");

			$.getJSON(this.apiUrl + "/" + id + "?callback=?", function(data) {
				//Fill form values
				$("#name").val(data.name);
				$("#email").val(data.email);
				$("#phone").val(data.phone);
				$("#username").val(data.username);
				$("#website").val(data.website);
				$("#address").val(data.address.street);
				$('#apartment').val(data.address.suite);
				$('#city').val(data.address.city);
				$("#zip").val(data.address.zipcode);
				$("#company").val(data.company.name);
				$("#catchphrase").val(data.company.catchPhrase);
				$("#bs").val(data.company.bs);
				$("#userId").val(id);
				$("#userModal").modal(); //Open modal
			}).fail(function() {
				//something went wrong
				putAlert("Error loading user!", 'danger');
			});
		}, this));
	}

	attachUserEvents() {
		this.attachRowEvents();
		$('#deleteUser').click($.proxy(function(event) {
			event.preventDefault();
			//Handle modal delete-button click
			var id = $(event.currentTarget).attr('data');
			this.deleteUser(id);
			$('#delModal').modal('hide');
		}, this));

		$('#openAddUser').click(function(event) {
			event.preventDefault();
			//Open add user modal
			//and reset form
			$("#userForm")[0].reset();
			$("#userId").val(0);
			$('#userModal').modal();
		});

		$('#userForm').submit($.proxy(function(event) {
			event.preventDefault();
			var form = $('#userForm');
			var data = form.serialize();
			var requiredFields = ['name', 'username']; //Required fields in form
			var _this = this;
			var validated = true;
			var method = "POST";
			var userId = '';
			var userSuccess = "New user added!";
			var userFail = "Error creating a new user!";

			$.each(requiredFields, function(key, val) {
				//Iterate through required fields
				var element = form.find("#" + val);
				if(element.val().length == 0) {
					validated = false;
					element.parent().addClass('has-error');
				} else {
					element.parent().removeClass('has-error');
				}
			});

			if(!validated) {
				return false;
			}

			if($("#userId").val() != 0) {
				//We are updating existing user
				//Set proper REST parameters and messages
				method = "PUT";
				userId = "/" + $("#userId").val();
				userSuccess = "User updated!";
				userFail = "Error updating user!";
			}

			$.ajax('http://jsonplaceholder.typicode.com/users' + userId, {
				method: method,
				data: data
			}).done(function(data) {
				if(method == "POST") {
					//We are inserting a new user, update table and attach events to buttons
					var row = $('<tr data="' + data.id + '">').append("<td class='name'>" + data.name + "</td><td class='username'>" + data.username + "</td><td class='email'>" + data.email + "</td>" + _this.editCells);
					table.row.add(row).draw();
					_this.attachRowEvents();
				}
				putAlert(userSuccess, 'success');
			}).fail(function() {
				putAlert(userFail, 'danger');
			}).always(function() {
				$('#userModal').modal('hide');
			});
		}, this));
	}
};

function putAlert(string, alertType) {
	var alert = '<div class="alert alert-' + alertType +' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + string + '</div>';
	$("#alerts").html(alert);
}